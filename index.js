console.log("Hello World")

// Global Objects
// Arrays

// let students = [
//     "Tony",
//     "Peter",
//     "Wanda",
//     "Vision",
//     "Loki"
// ]
// console.log(students)
// what array method can we use to add an item at the end of the array

// students.push("Thor")
// console.log(students)

// what array method can we use to add an item at the start of the array
// students.unshift("Steve")
// console.log(students)

// what array method does the opposite of push()?
// students.pop()
// console.log(students)

// what array method does the opposite of shift()?
// students.shift()
// console.log(students)

// what is the difference between splice() and slice()?
    // .splice() - removes and add items from a starting index (Mutator methods)
    // .slice() - copies a portion from starting index and returns new array from it (Non-Mutator methods)

// Another kind of array methods?
    // Iterator methods - loops over the items of an array

// forEach() - loops over items in an array and repeats a user-defined function
// map() - loops over items in an array and repeats a user-defined function AND returns a new array
// every() - loops and checks if all items satisfy a given condition, return a boolean value

// let arrNum = [15, 20, 25, 30, 11]

// check if every item in arrNum is divisible by 5
// let allDivisible

// arrNum.forEach(num =>{
//     if(num%5 === 0){
//         console.log(`${num} is divisible by 5`)
//     } else {
//         allDivisible = false
//     }
// })
// console.log(allDivisible)

// arrNum.pop()
// arrNum.push(35)
// console.log(arrNum)

// let divisibleBy5 = arrNum.every(num =>{
//     console.log(num)
//     return num % 5 === 0;
// })
// console.log(divisibleBy5)
// result: true

// Math
// Mathematical constants
// 8 pre-defined properties which can be called via the syntax Math.property (note that properties are case-sensitive)
// console.log(Math)
// console.log(Math.E) // Euler's number
// console.log(Math.PI) // PI
// console.log(Math.SQRT2) // square root of 2
// console.log(Math.SQRT1_2) // square root of 1/2
// console.log(Math.LN2) // natural logarithm of 2
// console.log(Math.LN10) // natural logarithm of 10
// console.log(Math.LOG2E) // base 2 logarithm of E
// console.log(Math.LOG10E) // base 10 logarithm of E

// methods
// console.log(Math.round(Math.PI)) // rounds to a nearest integer; result 3
// console.log(Math.ceil(Math.PI)) // rounds up to nearest integer; result 4
// console.log(Math.floor(Math.PI)) // rounds down to nearest integer; result 3
// console.log(Math.trunc(Math.PI)) // returns only the integer part (ES6 update) result 3

// method for returning the square root of a number
// console.log(Math.sqrt(3.14)) // result: 1.77
// console.log(Math.sqrt(Math.PI)) // result: 1.77\

// lowest value in a list of arguments
// console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4)) // result: -4

// highest value in a list of arguments
// console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4)) // result: -4

let students = [
    "John",
    "Joe",
    "Jane",
    "Jessie"
]
function addToEnd(arr,addString) {
    if (typeof(addString) == "string") {
        arr.push(addString)
        return console.log(arr)
    } else {
        return console.log("Error - can only add strings to an array")
    }
}

function addToStart(arr,addString) {
    if (typeof(addString) == "string") {
        arr.unshift(addString)
        return console.log(arr)
    } else {
        return console.log("Error - can only add strings to an array")
    }
}

function elementChecker(arr,checkString) {
    if (arr.length === 0) {
        return console.log(`Error - passed in array is empty`)
    } else if (arr.includes(checkString)) {
        return true
    } else {
        return false
    }
}

function checkAllStringsEnding(arr,char) {
    let status = true
    if (arr.length === 0) {
        return console.log(`Error - Array must not be empty`)
    } else if (typeof(char) !== 'string') {
        return console.log(`Error - 2nd argument must be of data type string`)
    } else if (char.length > 1) {
        return console.log(`Error - 2nd argument must be a single character`)
    }
    arr.forEach(elem =>{
        if (typeof(elem) !== 'string') {
            return console.log(`Error - All array elements must be strings`)
        } else if ((elem[elem.length-1] === char) && status == true) {
            status = true
        } else {
            status = false
        }
    })
    return status
}

function stringLengthSorter(arr){
    arr.forEach(elem =>{
        if (typeof(elem) !== 'string') {
            return console.log(`Error - All array elements must be strings`)
        }
    })
    let newArr = arr.sort((a,b) => a.length - b.length)
    return newArr
}